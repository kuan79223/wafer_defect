import os
import cv2
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtGui import QImage, QPixmap, QPainter

import wafer_ui
import sys
from PyQt5.QtWidgets import QPushButton, QGraphicsScene, QGraphicsView, QWidget, QGridLayout


class WaferView(QtWidgets.QWidget, wafer_ui.Ui_Form):
    SIGNAL_DETECTION = pyqtSignal(list)

    def __init__(self):
        super(WaferView, self).__init__()
        self.setupUi(self)
        self.SIGNAL_DETECTION.connect(self.set_detection_list)
        self.img_path = r'D:\SHA_img\TP'
        self.row_nums = []
        self.sorted_dict = {}
        self.btn_names = []
        self.data = []

        self.button = None
        self.gridLayout = QGridLayout()
        self.widget = QWidget()
        self.create_btn()
        self.detections = []
        self.scene = QGraphicsScene()

        self.lw_detect_list.clicked.connect(self.click_show_image)

        self.gsv_detected_obj.setMouseTracking(True)
        self.gsv_detected_obj.setRenderHints(QPainter.Antialiasing | QPainter.SmoothPixmapTransform)
        self.gsv_detected_obj.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        self.gsv_detected_obj.setResizeAnchor(QGraphicsView.AnchorUnderMouse)

    def create_btn(self):

        images = os.listdir(self.img_path)

        self.data = [os.path.splitext(img)[0] for img in images]  # 只有檔名

        result_dict = {}
        col_nums = []

        for d in self.data:
            # 先去除後面的[], 在處理前面的列與欄
            key, *values = d.split("_")
            col, row = key.split("-")
            # 使用列當作主字典的key
            if col not in result_dict:
                result_dict[col] = {}
            # 使用欄當作子字典的 key
            if row not in result_dict[col]:
                result_dict[col][row] = []
            # 最後再由主 + 子字典去append檔案數據
            result_dict[col][row].append(d)

            '''字典型態為 { 列 : { 欄 : [ 檔案名稱 ...] } }'''
            for mk in list(result_dict.keys()):
                # print(mk)
                self.row_nums.append(int(mk))
            for k in list(result_dict[col].keys()):
                # print(k)
                col_nums.append(int(k))

        # 使用 .items() 將各個子字典劃分成 tuple，針對   (key, {key,[]})    做切割取得主 key 做排序
        self.sorted_dict = dict(sorted(result_dict.items(), key=lambda x: int(x[0].split(' ')[0])))

        print(self.sorted_dict)

        for row_idx in range(1, 14):
            for col_idx in range(1, 115):

                print(f'按鈕 {col_idx}-{row_idx}')

                if str(col_idx) in list(self.sorted_dict.keys()) and str(row_idx) in self.sorted_dict[str(col_idx)]:

                    self.button = QPushButton('')
                    self.button.setObjectName(str(col_idx) + '_' + str(row_idx))
                    self.btn_names.append(str(col_idx) + '_' + str(row_idx))

                    self.button.setStyleSheet('''
                                            background-color:blue;
                                            color:black;
                                            width: 10px;
                                            height: 10px
                                        ''')
                else:
                    # print(str(col_idx) + '-' + str(row_idx), '白色按鈕')
                    self.button = QPushButton('')
                    self.button.setObjectName(str(col_idx) + '_' + str(row_idx))
                    self.button.setStyleSheet('''
                                           background-color:white;
                                           color:black;
                                           width: 10px;
                                            height: 10px
                                            ''')

                self.gridLayout.addWidget(self.button, col_idx, row_idx)
                self.button.clicked.connect(self.button_click)

                self.widget.setLayout(self.gridLayout)
                self.scrollArea.setWidget(self.widget)

    def button_click(self):
        # 使用 sender().objectName() 設定按鈕的名稱
        btn = self.sender().objectName()
        objectname = btn.replace('_', '-')
        # print('觸發', objectname)
        row, col = btn.split('_')

        # 判斷當前 row 存在於字典內的 row
        if row in self.sorted_dict.keys():
            # 判斷當前 col 存在於字典內的 col
            if col in self.sorted_dict[row].keys():
                # [['98-3'], ['98-9', '98-9_[1]', '98-9_[2]', '98-9_[3]', '98-9_[4]', '98-9_[5]', '98-9_[6]']]
                for value in self.sorted_dict[row].values():
                    # print(f'欄的陣列數值{value}\n')
                    for filename in value:
                        # print('陣列元素', filename)
                        if objectname in filename:
                            self.scene.clear()  # 每次匹配成功清空畫面
                            self.SIGNAL_DETECTION.emit(value)
                            break  # 使用 break 阻斷迴圈，才不會找到目標了繼續往後循環
                        else:
                            print('清空畫面')
                            self.scene.clear()

                            break

            else:
                self.lw_detect_list.clear()
                self.scene.clear()

    def click_show_image(self):
        text = self.lw_detect_list.currentItem().text()  # 取得項目內容

        filename = text + '.jpeg'
        print(filename)
        img = cv2.imread(os.path.join(self.img_path, filename))
        # cv2.imshow('img', img)
        # cv2.waitKey(1)
        height, width = img.shape[:2]
        q_img = QImage(bytes(img), width, height, 3 * width, QImage.Format_BGR888)
        pixmap = QPixmap(q_img)
        self.set_scene_view(pixmap)

    def set_detection_list(self, datas):

        self.lw_detect_list.clear()
        self.lw_detect_list.addItems(datas)

    def set_scene_view(self, pixmap):

        self.scene.addPixmap(pixmap)
        self.gsv_detected_obj.setScene(self.scene)
        # 讓場景自適應視圖 兩個語法都可以用，第一個參數代表 場景的邊界框
        self.gsv_detected_obj.fitInView(self.scene.itemsBoundingRect(), QtCore.Qt.KeepAspectRatio)
        # self.gsv_detected_obj.fitInView(self.scene.sceneRect(), Qt.KeepAspectRatio)


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    form = WaferView()
    form.show()
    sys.exit(app.exec_())